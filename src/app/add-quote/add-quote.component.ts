import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-add-quote',
  templateUrl: './add-quote.component.html',
  styleUrls: ['./add-quote.component.css']
})
export class AddQuoteComponent implements OnInit {

  category = '';
  author = '';
  text = '';
  title = 'Add quote';
  quoteId: string | null = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {}

  addQuote() {
    const category = this.category;
    const author = this.author;
    const text = this.text;
    this.route.params.subscribe((params: Params) => {
      if(params['id']) {
        this.title = 'Change quote';
        this.quoteId = params['id'];
        const body = {category, author, text};
        this.http.put(`https://js-group-13-default-rtdb.firebaseio.com/quotes/` + this.quoteId + `.json`, body)
          .subscribe();
      } else {
        const body = {category, author, text};
        this.http.post('https://js-group-13-default-rtdb.firebaseio.com/quotes.json', body)
          .subscribe();
      }
      this.author = '';
      this.text = '';
    })
  }

}
