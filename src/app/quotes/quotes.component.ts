import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quote } from '../shared/qoute.model';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {

  quotes!: Quote[];
  quote: Quote | null = null;
  category: string | null = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.category = params['category'];
      if (!this.category) {
        this.requestQuotes();
      } else {
        this.requestQuotesByCategory();
      }
    });
  }

  deleteQuote(i: number) {
    this.http.delete<Quote>(`https://js-group-13-default-rtdb.firebaseio.com/quotes/` + this.quotes[i].id + `.json`)
      .subscribe();
    this.requestQuotes();
  }

  requestQuotes() {
    this.http.get<{ [id: string]: Quote }>('https://js-group-13-default-rtdb.firebaseio.com/quotes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(id, quoteData.author, quoteData.category, quoteData.text)
        });
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
      })
  }

  requestQuotesByCategory() {
    this.http.get<{ [id: string]: Quote }>('https://js-group-13-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="' + this.category + '"')
      .pipe(map(result => {

        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(id, quoteData.author, quoteData.category, quoteData.text)
        });
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
      })
  }

}

//Author: People
// Kolobok hanged himself.
// Delete
//   Edit
// Author: People
// Buratino drowned.
// Delete
//
